# Cellulant.io : Checkout Encryption
PHP Library to Encrypt Payload 
[`Get Started`](https://cellulant-prod-portal.apigee.io/libraries/php)

---

To ensure the highest level of security, Cellulant only accepts encrypted data in checkout request. Encrption library can be use with any PHP framework. [Composer](https://getcomposer.org/) is an application-level package manager for the PHP programming language that provides a standard format for managing dependencies of PHP software and required libraries. In below examples, we will use the **Composer** to create a realistic use cases.

## Project setup instructions

**Step 1**: Create a project directory 
```
$ mkdir /var/www/html/checkout-demo
```

**Step 2**: Go into the project directory. 
```
$ cd checkout-demo
```

**Step 3**: Initialize composer
```
$ composer init
```

**Step 4**: Download and install dependency. 
```
$ composer require cellulant/checkout_encryption:dev-master
```

**Step 5**: Create a file CheckoutEncryption.php and paste in the following PHP code.
```
// File name : CheckoutEncryption.php

<?php
require_once __DIR__ . '/vendor/autoload.php';
use checkout\encryption\Encryption;
// Alternatively configure this on your web server(apache/nginx) to avoid CORS error
header("Access-Control-Allow-Origin: *");

class CheckoutEncryption {

    private $accessKey;
    private $ivKey;
    private $secretKey;
    private $request;

    public function __construct() {
        $this->accessKey = "<YOUR_ACCESS_KEY>";
        $this->ivKey = "<YOUR_IV_KEY>";
        $this->secretKey = "<YOUR_SECRET_KEY>";

        // sample payload object 
        $jsonObject = '{
             "merchantTransactionId":"TXN10004",
             "customerFirstName":"Test",
             "customerLastName":"User",
             "msisdn":123456,
             "customerEmail":"testuser@gmail.com",
             "requestAmount":10.0,
             "currencyCode":"KSH",
             "accountNumber":"12345",
             "serviceCode":"DEMOSERVICE",
             "dueDate":"2021-11-21",
             "requestDescription":"Test payment",
             "countryCode":"KEN",
             "languageCode":"EN",
             "paymentOptionCode":"Mpesa",
             "successRedirectUrl":"http://abc.com/success",
             "failRedirectUrl":"http://abc.com/fail",
             "pendingRedirectUrl":"http://abc.com/pending",
             "callbackUrl":"http://abc.com/callback",
             "charge_beneficiaries":null
        }';

        $this->request = json_decode($jsonObject, true);

    }

    public function processEncryption() {
        $obj = new Encryption();
        $encryptedParams = $obj->encrypt($this->ivKey, $this->secretKey, $this->request);
        $result = [
            'params' => $encryptedParams,
            'accessKey' => $this->accessKey,
            'countryCode' => $this->request['country_code']
        ];

        echo json_encode($result);
    }

}

$class = new CheckoutEncryption();
$class->processEncryption();

?>

```

---

## Reference

- [Packagist](https://packagist.org/packages/cellulant/checkout_encryption).
